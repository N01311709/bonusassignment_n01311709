﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_N01311709
{
    public partial class BonusAssignments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Cartesian_Validation(object source, ServerValidateEventArgs args)
        {
            if (xPrompt.Text != "0" || yPrompt.Text != "0" /* || xPrompt != null || yPrompt != null*/ )
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Smartesian(object sender, EventArgs e)
        {
            int x = int.Parse(xPrompt.Text);
            int y = int.Parse(yPrompt.Text);
            string quadResult = "";
            if (x > 0 && y > 0)
            {
                quadResult = "1";
            } else if (x < 0 && y > 0)
            {
                quadResult = "2";
            } else if (x < 0 && y < 0)
            {
                quadResult = "3";
            } else if (x > 0 && y < 0)
            {
                quadResult = "4";
            }

            whatQuad.InnerHtml = "These coordinates put you in quadrant " + quadResult;
        }

        protected void Divisible_Validation(object source, ServerValidateEventArgs args)
        {
            if (int.Parse(primePrompt.Text) <= 1)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Sizzable(object sender, EventArgs e)
        {
            int myNum = int.Parse(primePrompt.Text);
            
            for (int i = (myNum - 1); i >= 2; i--)
            {
            if (myNum % i == 0)
            {
                isPrime.InnerHtml = myNum.ToString() + " is not a prime number!";
                return;
            } else
            {
                isPrime.InnerHtml = myNum.ToString() + " is a prime number!";
            }

            }
        }

       

        protected void Bling(object sender, EventArgs e)
        {
            string blingString = palinPrompt.Text.ToLower();
            blingString = blingString.Replace(" ",string.Empty);

            int stringLength = blingString.Length;

            for (int i = 0; i < stringLength/2; i++)
            {
                if (blingString[i] != blingString[stringLength-i-1])
                {
                    isPalin.InnerHtml = blingString + " is not a palindrome.";
                    return;
                } else
                {
                    isPalin.InnerHtml = blingString + " is a palindrome!";
                }
            }
        }
    }
}