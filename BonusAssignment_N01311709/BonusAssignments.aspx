﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BonusAssignments.aspx.cs" Inherits="BonusAssignment_N01311709.BonusAssignments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bonus Assignments</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Bonus Assignment</h1>
            <p>Any errors in validation will appear here.</p>
            <asp:ValidationSummary ID="validationsummary" runat="server" />
            <h2>1. Cartesian Smartesian</h2>
            <p>
                <asp:Label runat="server" ID="xPromptLabel" AssociatedControlID="xPrompt" Text="Enter the x coordinate here."></asp:Label>
            </p>
            <p>
                <asp:CustomValidator runat="server" ErrorMessage="x cannot be 0." ControlToValidate="xPrompt" OnServerValidate="Cartesian_Validation"></asp:CustomValidator>
                <asp:TextBox runat="server" TextMode="Number" ID="xPrompt" Placeholder="No zeroes please."></asp:TextBox>
            </p>
            <p>
                <asp:Label runat="server" ID="yPromptLabel" AssociatedControlID="yPrompt" Text="Enter the y coordinate here"></asp:Label>
            </p>
            <p>
                <asp:CustomValidator runat="server" ErrorMessage="y cannot be 0." ControlToValidate="yPrompt" OnServerValidate="Cartesian_Validation"></asp:CustomValidator>
                <asp:TextBox runat="server" TextMode="Number" ID="yPrompt" Placeholder="No zeroes please."></asp:TextBox>
            </p>

            <asp:Button runat="server" ID="submitCart" OnClick="Smartesian" Text="Tell me the quadrant"/>

            <div runat="server" id="whatQuad">
                ???
            </div>

            <h2>2. Divisible Sizzable</h2>
            <p>
                <asp:Label runat="server" ID="primePromptLabel" AssociatedControlID="primePrompt" Text="Enter a positive number here"></asp:Label>
            </p>
            <p>
                <asp:CustomValidator runat="server" ErrorMessage="Must be a positive value." ControlToValidate="primePrompt" OnServerValidate="Divisible_Validation"></asp:CustomValidator>
                <asp:TextBox runat="server" TextMode="Number" ID="primePrompt" Placeholder="Provide a number greater than 1."></asp:TextBox>
            </p>       
            <asp:Button runat="server" ID="submitDivis" OnClick="Sizzable" Text="Is this a prime number?"/>
            <div runat="server" id="isPrime">
                ???
            </div>
            <h2>3. String Bling</h2>
            <p>
                <asp:Label runat="server" ID="palinPromptLabel" AssociatedControlID="palinPrompt" Text="Enter a string here"></asp:Label>
               <!-- I modified the validation expression to tolerate spaces between words. I was helped by this link: https://stackoverflow.com/questions/23169911/regex-validation-allow-only-character-and-space-in-asp-net -->
                <asp:RegularExpressionValidator runat="server" ID="palinPrompt_Validation" ValidationExpression="^[a-zA-Z ]+$" ControlToValidate="palinPrompt" ErrorMessage="Enter a string please."></asp:RegularExpressionValidator>
                <asp:TextBox runat="server" ID="palinPrompt"></asp:TextBox>
            </p>      
            <asp:Button runat="server" ID="submitString" OnClick="Bling" Text="Is this string palindromic?"/>
            <div runat="server" id="isPalin">
                ???
            </div>
        </div>
    </form>
</body>
</html>
